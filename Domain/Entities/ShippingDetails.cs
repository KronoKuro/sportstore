﻿using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace Domain.Entities
{
    public class ShippingDetails
    {
        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введите адресс")]
        public string Line1 {get; set;}

        public string Line2 { get; set; }
        public string Line3 { get;set; }

        [Required(ErrorMessage ="Please введите город")]
        public string City { get; set; }

         [Required(ErrorMessage = "Please enter a область name")]
        public string State { get; set; }

        public string Zip { get; set; }

        [Required(ErrorMessage = "Введите имя")]
        public string Country { get; set; }

        public bool GiftWrap { get; set; }


    }
}