﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using System.Configuration;

namespace Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        
        public EFDbContext()
        {
            Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["EFDbContext"].ConnectionString;
        }
        public DbSet<Product> Products { get; set; }
    }
}
