﻿using System;
using  Moq;
using Domain.Abstract;
using Domain.Entities;
using WebUI.Controllers;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class AdminTests
    {
        [TestMethod]
        public void Index_Contains_All_Products()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product{ProductId = 1, Name = "Р1"},
                new Product{ProductId = 2, Name = "Р2"},
                new Product{ProductId = 3, Name = "Р3"},
            }.AsQueryable());
            AdminController target = new AdminController(mock.Object);
            Product[] result = ((IEnumerable<Product>) target.Index().ViewData.Model).ToArray();
            Assert.AreEqual(result.Length,3);
            Assert.AreEqual("Р1", result[0].Name);
            Assert.AreEqual("Р2", result[1].Name);
            Assert.AreEqual("Р3", result[2].Name);


        }

        [TestMethod]
        public void Can_Edit_Product()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product {ProductId = 1, Name = "h1"},
                new Product {ProductId = 2, Name = "h2"},
                new Product {ProductId = 3, Name = "h3"},
            }.AsQueryable());
            AdminController target = new AdminController(mock.Object);
            Product p1 = target.Edit(1).ViewData.Model as Product;
            Product p2 = target.Edit(2).ViewData.Model as Product;
            Product p3 = target.Edit(3).ViewData.Model as Product;

            Assert.AreEqual(1, p1.ProductId);
            Assert.AreEqual(2, p2.ProductId);
            Assert.AreEqual(3, p3.ProductId);
        }

        [TestMethod]
        public void Cannot_Edit_Nonexistens_Product()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product {ProductId = 1, Name = "р1"},
                new Product {ProductId = 2, Name = "р2"},
                new Product {ProductId = 3, Name = "р3"},
            }.AsQueryable());
            AdminController targer = new AdminController(mock.Object);
            Product result = (Product)targer.Edit(4).ViewData.Model;
            Assert.IsNull(result);
        }

        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            AdminController target = new AdminController(mock.Object);
            Product product = new Product {Name = "Test"};
            ActionResult result = target.Edit(product);
            mock.Verify(m => m.SaveProduct(product));
            Assert.IsNotInstanceOfType(result, typeof(ViewResult));

        }

        [TestMethod]
        public void Cannot_Save_Invalid_Changes()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            AdminController target = new AdminController(mock.Object);
            Product product = new Product{ Name = "Test"};
            target.ModelState.AddModelError("error", "error");
            ActionResult result = target.Edit(product);
            mock.Verify(m => m.SaveProduct(It.IsAny<Product>()), Times.Never);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Can_Delete_Valid_Products()
        {
            Product prod = new Product{ ProductId = 2, Name = "Test"};
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product {ProductId = 1, Name = "h1"},
                prod,
                new Product {ProductId = 3, Name = "h3"},
            }.AsQueryable());
            AdminController target = new AdminController(mock.Object);
            target.Delete(prod.ProductId);
            mock.Verify(m => m.DeleteProduct(prod.ProductId));
        }

    }
}
