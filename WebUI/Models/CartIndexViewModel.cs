﻿using Domain.Entities;
using Domain.Abstract;
using WebUI.Models;

namespace WebUI.Models
{
    public class CartIndexViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}